package com.epam.gomel.tat.lesson_7.bo.mail;

import com.epam.gomel.tat.lesson_7.utils.MyFileUtils;
import com.epam.gomel.tat.lesson_7.utils.RandomText;

import java.io.File;

/**
 * Created by Natashka on 22.03.2015.
 */
public class LetterBuilder {
    public static String receiver = "natashka.test@yandex.ru";

    public static Letter getLetter(){
        Letter letter = new Letter(receiver,RandomText.randomStringShort(),RandomText.randomStringLong(),null);
        return letter;
    }

    public static Letter getLetterAttach(){
        Letter letter = getLetter();
        letter.setAttachment(MyFileUtils.createNewFile(new File(RandomText.randomStringShort() + ".txt")).getAbsolutePath());
        return letter;
    }

}
