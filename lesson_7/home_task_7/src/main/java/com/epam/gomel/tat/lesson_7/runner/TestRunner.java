package com.epam.gomel.tat.lesson_7.runner;

import com.epam.gomel.tat.lesson_7.GlobalConfig;
import com.epam.gomel.tat.lesson_7.reporting.CustomTestNGListener;
import com.epam.gomel.tat.lesson_7.reporting.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Natashka on 26.03.2015.
 */
public class TestRunner {

    public static void main(String[] args) {
        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
        try {
            Logger.info("Parse the arguments...");
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.error("Can't parse the arguments!");
            e.printStackTrace();
        }
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNGListener());
        List<String> suites = new ArrayList<String>();
        for (String suite : GlobalConfig.getInstance().getSuites()) {
                 suites.add(suite);
        }
        testNG.setTestSuites(suites);
        testNG.run();
    }

}

