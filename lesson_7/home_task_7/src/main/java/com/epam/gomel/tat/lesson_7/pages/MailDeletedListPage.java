package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class MailDeletedListPage extends AbstractBasePage {

    public static final String DELETED_SUBJECT_LOCATOR_PATTERN = "//label[text()='Удалённые']//ancestor::div[@class='block-messages']//span[@class='b-messages__subject'][text()='%s']";

    public boolean isMailPresent(String text) {
        return browser.isPresent(By.xpath(String.format(DELETED_SUBJECT_LOCATOR_PATTERN, text)));
    }
}
