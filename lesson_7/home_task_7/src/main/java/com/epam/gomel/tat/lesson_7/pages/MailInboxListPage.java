package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class MailInboxListPage extends AbstractBasePage {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final String INBOX_CHECKBOX_LOCATOR_PATTERN = "//span[@class='js-messages-title-dropdown-name']//ancestor::div[@class='block-messages']//span[@class='b-messages__subject'][text()='%s']//ancestor::div[contains(@class, 'b-messages__message')]//input[@type='checkbox']";
    public static final String INBOX_SUBJECT_LOCATOR_PATTERN ="//span[@class='js-messages-title-dropdown-name']//ancestor::div[@class='block-messages']//span[@class='b-messages__subject'][text()='%s']";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-params='toolbar=1&toolbar.button=delete']");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-params='toolbar=1&toolbar.button=spam']");


    public ComposeMailPage openComposeMail() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailboxBasePage deleteCheckedMail(String text){
        browser.waitForVisible(By.xpath(String.format(INBOX_SUBJECT_LOCATOR_PATTERN, text)));
        browser.click(By.xpath(String.format(INBOX_CHECKBOX_LOCATOR_PATTERN,text)));
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForNotVisible(By.xpath(String.format(INBOX_SUBJECT_LOCATOR_PATTERN, text)));
        return new MailboxBasePage();
    }

    public MailboxBasePage markMailAsSpam(String text) {
        browser.waitForVisible((By.xpath(String.format(INBOX_SUBJECT_LOCATOR_PATTERN, text))));
        browser.click(By.xpath(String.format(INBOX_CHECKBOX_LOCATOR_PATTERN,text)));
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForNotVisible(By.xpath(String.format(INBOX_SUBJECT_LOCATOR_PATTERN, text)));
        return new MailboxBasePage();
    }

    public boolean isMailPresent(String text) {
        return browser.isPresent(By.xpath(String.format(INBOX_SUBJECT_LOCATOR_PATTERN,text)));
    }
}
