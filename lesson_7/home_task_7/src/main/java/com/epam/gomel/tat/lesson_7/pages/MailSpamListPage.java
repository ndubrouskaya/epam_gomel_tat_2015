package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class MailSpamListPage extends AbstractBasePage {

    public static final String SPAM_SUBJECT_LOCATOR_PATTERN = "//label[text()='Спам']//ancestor::div[@class='block-messages']//span[@class='b-messages__subject'][text()='%s']";
    public static final String SPAM_CHECKBOX_LOCATOR_PATTERN = "//label[text()='Спам']//ancestor::div[@class='block-messages']//span[@class='b-messages__subject'][text()='%s']//ancestor::div[contains(@class, 'b-messages__message')]//input[@type='checkbox']";
    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='notspam']");

    public MailboxBasePage markMailNotSpam(String text) {
        browser.waitForVisible(By.xpath(String.format(SPAM_SUBJECT_LOCATOR_PATTERN, text)));
        browser.click(By.xpath(String.format(SPAM_CHECKBOX_LOCATOR_PATTERN,text)));
        browser.click(NOT_SPAM_BUTTON_LOCATOR);
        browser.waitForNotVisible(By.xpath(String.format(SPAM_SUBJECT_LOCATOR_PATTERN, text)));
        return new MailboxBasePage();
    }

    public boolean isMailPresent(String text) {
        return browser.isPresent(By.xpath(String.format(SPAM_SUBJECT_LOCATOR_PATTERN,text)));
    }
}
