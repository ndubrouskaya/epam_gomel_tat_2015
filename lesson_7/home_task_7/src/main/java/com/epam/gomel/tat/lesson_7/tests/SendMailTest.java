package com.epam.gomel.tat.lesson_7.tests;

import com.epam.gomel.tat.lesson_7.bo.common.Account;
import com.epam.gomel.tat.lesson_7.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_7.bo.mail.Letter;
import com.epam.gomel.tat.lesson_7.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_7.service.LoginGuiService;
import com.epam.gomel.tat.lesson_7.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 20.03.2015.
 */
public class SendMailTest extends BaseTest{

    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getLetter();
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account account = AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Success login to yandex mail")
    public void successLogin(){
        loginGuiService.loginToAccountMailbox(account);
    }

    @Test(description = "Success send mail")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent(){
        mailGuiService.checkMailInSent(letter);
    }
}
