package com.epam.gomel.tat.lesson_7.reporting;

/**
 * Created by Natashka on 24.03.2015.
 */
public class Logger {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void fatal(String s){
        log.fatal(s);
    }
    public static void error(String s){
        log.error(s);
    }
    public static void warn(String s){
        log.warn(s);
    }
    public static void info(String s){
        log.info(s);
    }
    public static void debug(String s){
        log.debug(s);
    }
     public static void trace(String s){
        log.trace(s);
    }

    public  static void fatal(String s, Throwable t){
        log.fatal(s,t);
    }
    public  static void error(String s, Throwable t){
        log.error(s, t);
    }
    public  static void warn(String s, Throwable t){
        log.warn(s, t);
    }
    public  static void info(String s, Throwable t){
        log.info(s, t);
    }
    public  static void debug(String s, Throwable t){
        log.debug(s, t);
    }
    public  static void trace(String s, Throwable t){
        log.trace(s,t);
    }
}
