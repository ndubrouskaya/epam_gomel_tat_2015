package com.epam.gomel.tat.lesson_7.exeptions;

/**
 * Created by Natashka on 24.03.2015.
 */
public class TestLoginException extends TestCommonRuntimeException {

    public TestLoginException(String message) {
        super(message);
    }
}
