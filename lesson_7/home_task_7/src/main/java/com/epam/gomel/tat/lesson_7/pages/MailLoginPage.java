package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;


/**
 * Created by Natashka on 20.03.2015.
 */
public class MailLoginPage extends AbstractBasePage {

    public static final String BASE_URL = "http://www.mail.yandex.ru";

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//span[contains(@class, 'auth-form')]/button");

    public MailLoginPage open(){
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password){
        super.browser.type(LOGIN_INPUT_LOCATOR,login);
        super.browser.type(PASSWORD_INPUT_LOCATOR, password);
        super.browser.submit(ENTER_BUTTON_LOCATOR);
        return new MailboxBasePage();

    }

    public YandexPassportPage unsuccessLogin(String login, String password){
        super.browser.type(LOGIN_INPUT_LOCATOR,login);
        super.browser.type(PASSWORD_INPUT_LOCATOR, password);
        super.browser.submit(ENTER_BUTTON_LOCATOR);
        return new YandexPassportPage();

    }

}
