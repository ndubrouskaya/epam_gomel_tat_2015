package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class YandexPassportPage extends AbstractBasePage{

    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg']");
    public static final String ERROR_MESSAGE_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public String getErrorMessage() {
        return browser.getText(ERROR_MESSAGE_LOCATOR);
    }
}
