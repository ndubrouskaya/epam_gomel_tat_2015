package com.epam.gomel.tat.lesson_7.tests;

import com.epam.gomel.tat.lesson_7.GlobalConfig;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

/**
 * Created by Natashka on 02.04.2015.
 */
public class BaseTest {
    @BeforeSuite
    public void befSuite(ITestContext context) {
        context.getSuite().getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode().getAlias());
        context.getSuite().getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }
}
