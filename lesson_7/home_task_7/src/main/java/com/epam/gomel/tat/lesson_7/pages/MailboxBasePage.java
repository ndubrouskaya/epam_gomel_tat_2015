package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class MailboxBasePage extends AbstractBasePage {

    public static final By ACCOUNT_LINK_LOCATOR = By.xpath("//span[contains (@class, 'header-user-name')]");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//div[@class='block-left-box']//a[@href='#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By DELETED_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");

    public MailInboxListPage openInboxListPage(){
        browser.waitForPresent(INBOX_LINK_LOCATOR);
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSentListPage openSentListPage(){
        browser.waitForPresent(SENT_LINK_LOCATOR);
        browser.click(SENT_LINK_LOCATOR);
        return new MailSentListPage();
    }

    public MailDeletedListPage openDeletedListPage(){
        browser.waitForPresent(DELETED_LINK_LOCATOR);
        browser.click(DELETED_LINK_LOCATOR);
        return new MailDeletedListPage();
    }

    public MailSpamListPage openSpamListPage(){
        browser.waitForPresent(SPAM_LINK_LOCATOR);
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamListPage();
    }

    public String getUserEmail() {
        browser.waitForPresent(ACCOUNT_LINK_LOCATOR);
        return browser.getText(ACCOUNT_LINK_LOCATOR);
    }
}
