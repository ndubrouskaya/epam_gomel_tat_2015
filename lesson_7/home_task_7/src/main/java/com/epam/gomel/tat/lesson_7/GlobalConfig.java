package com.epam.gomel.tat.lesson_7;

import com.epam.gomel.tat.lesson_7.ui.BrowserType;
import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.List;

/**
 * Created by Natashka on 26.03.2015.
 */
public class GlobalConfig {

    public static final String DOWNLOAD_DIR = FileUtils.getTempDirectoryPath();

    @Option(name = "-host", usage = "hostname")
    private String host = "localhost";

    @Option(name = "-port", usage = "port number")
    private int port = 4444;

    @Option(name = "-bt", usage = "browser type")
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", usage = "thread count")
    private int threadCount = 1;

    @Option(name = "-suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites;

    private static GlobalConfig instance;

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

