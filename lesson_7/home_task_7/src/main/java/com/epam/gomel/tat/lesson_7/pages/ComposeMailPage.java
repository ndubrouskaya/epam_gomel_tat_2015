package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class ComposeMailPage extends AbstractBasePage {

    public static final By TO_INPUT_FIELD_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_FIELD_LOCATOR = By.name("subj");
    public static final By MAIL_TEXTAREA_LOCATOR = By.id("compose-send");
    public static final By ATTACH_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent, String attachment){
        browser.type(TO_INPUT_FIELD_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_FIELD_LOCATOR, mailSubject);
        browser.type(MAIL_TEXTAREA_LOCATOR, mailContent);
        if (attachment != null) {
            browser.attachFile(ATTACH_BUTTON_LOCATOR, attachment);
        }
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}
