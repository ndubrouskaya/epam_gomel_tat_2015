package com.epam.gomel.tat.lesson_7.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 20.03.2015.
 */
public class MailSentListPage extends AbstractBasePage {

    public static final String SENT_SUBJECT_LOCATOR_PATTERN = "//label[text()='Отправленные']//ancestor::div//span[@class='b-messages__subject'][text()='%s']";

    public LetterContentPage openLetter(String text) {
        browser.click(By.xpath(String.format(SENT_SUBJECT_LOCATOR_PATTERN,text)));
        return new LetterContentPage();
    }
}
