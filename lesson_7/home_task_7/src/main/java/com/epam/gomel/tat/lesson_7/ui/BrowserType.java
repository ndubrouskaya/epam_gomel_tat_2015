package com.epam.gomel.tat.lesson_7.ui;

/**
 * Created by Natashka on 26.03.2015.
 */
public enum  BrowserType {

    REMOTE("firefox*"),
    FIREFOX ("firefox"),
    CHROME ("chrome");

    private String alias;

    BrowserType (String alias){
        this.alias = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for (BrowserType type : BrowserType.values()) {
            if (type.getAlias().equals(alias.toLowerCase())) {
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}
