package com.epam.gomel.tat.lesson_7.service;

import com.epam.gomel.tat.lesson_7.bo.mail.Letter;
import com.epam.gomel.tat.lesson_7.exeptions.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_7.pages.*;
import com.epam.gomel.tat.lesson_7.reporting.Logger;
import com.epam.gomel.tat.lesson_7.GlobalConfig;
import com.epam.gomel.tat.lesson_7.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Natashka on 22.03.2015.
 */
public class MailGuiService {

    public void sendMail(Letter letter) {
        Logger.info("Send mail to " + letter.getReceiver() + " with subject " + letter.getSubject());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().openComposeMail().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttachment());
    }

    public void checkMailInSent(Letter letter) {
        Logger.info("Check if sent mail with subject " + letter.getSubject() + " present in sent list");
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterContent = mailbox.openSentListPage().openLetter(letter.getSubject());
        String receiver = letterContent.getLetterReceiver();
        String subject = letterContent.getLetterSubject();
        String content = letterContent.getLetterContent();
        StringBuilder errorBuilder = new StringBuilder();
        if (receiver == null || !receiver.equals(letter.getReceiver())) {
            errorBuilder.append(subject + " - is incorrect! Receiver of the sent mail: '" + letter.getReceiver() + "'");
        }
        if (subject == null || !subject.equals(letter.getSubject())) {
            errorBuilder.append(subject + " - is incorrect! Subject of the sent mail: '" + letter.getSubject() + "'");
        }
        if (content == null || !content.equals(letter.getContent())) {
            errorBuilder.append("Content of sent mail is incorrect!");
        }
        if (letter.getAttachment() != null) {
            Logger.info("Download attached file");
            letterContent.downloadAttachedFile();
            File attachedFile = new File(letter.getAttachment());
            File downloadFile = new File(GlobalConfig.DOWNLOAD_DIR + attachedFile.getName());
            MyFileUtils.waitForDownload(downloadFile);
            try {
                if (!FileUtils.contentEquals(new File(letter.getAttachment()), downloadFile)) {
                    Logger.error("Files different!");
                    errorBuilder.append("Files different!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!errorBuilder.toString().equals("")) {
            Logger.error(errorBuilder.toString());
            throw new TestCommonRuntimeException(errorBuilder.toString());
        }
    }

    public void markAsSpam(Letter letter) {
        Logger.info("Mark mail with subject " + letter.getSubject() + " as spam");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().markMailAsSpam(letter.getSubject());
    }

    public void checkMailInSpam(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in spam list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailSpamListPage mailSpam = mailbox.openSpamListPage();
        if (mailSpam.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in spam!");
            throw new TestCommonRuntimeException("No message in spam!");
        }
    }

    public void deleteMail(Letter letter) {
        Logger.info("Delete mail with subject " + letter.getSubject());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().deleteCheckedMail(letter.getSubject());
    }

    public void checkMailInDeleted(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in deleted list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailDeletedListPage mailDeleted = mailbox.openDeletedListPage();
        if (mailDeleted.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in deleted!");
            throw new TestCommonRuntimeException("No message in deleted!");
        }
    }

    public void markAsNotSpam(Letter letter) {
        Logger.info("Mark mail with subject " + letter.getSubject() + " as not spam");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openSpamListPage().markMailNotSpam(letter.getSubject());
    }

    public void checkMailInInbox(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in inbox list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailInboxListPage mailInbox = mailbox.openInboxListPage();
        if (mailInbox.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in inbox!");
            throw new TestCommonRuntimeException("No message in inbox!");
        }
    }
}
