package com.epam.tat.lesson7.page;

import com.epam.tat.lesson7.browser.Browser;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class GooglePage {

    private static final String GOOGLE_HOME = "http://google.ru" ;

    public static GooglePage open() {
        Browser.get().open(GOOGLE_HOME);
        return new GooglePage();
    }
}
