package com.epam.tat.lesson8.test.plunker;

import com.epam.tat.lesson8.browser.Browser;
import com.epam.tat.lesson8.test.BaseTest;
import com.epam.tat.lesson8.utils.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class PlunkerTest extends BaseTest {

    private static final String PLNKR = "http://run.plnkr.co/plunks/VUOWcZ7iZbd7UG2yDS1A/";

    @Test(description = "Check implicit wait works")
    public void waitForElementImplicitlySuccess() {
        Browser.get().open(PLNKR);
        Browser.get().configImplicitWait(5);
        Logger.info("Text: " + Browser.get().getText(By.id("title")));
    }

    @Test(description = "Check implicit wait failed", expectedExceptions = NoSuchElementException.class)
    public void waitForElementImplicitlyFailed() {
        Browser.get().open(PLNKR);
        Browser.get().configImplicitWait(5);
        Logger.info("Text: " + Browser.get().getText(By.id("failed")));
    }

    @Test
    public void waitForElementExplicitlyPresent() {
        Browser.get().open(PLNKR);
        Browser.get().configImplicitWait(0);
        Browser.get().waitForPresent(By.id("title"));
    }

    @Test
    public void waitForElementExplicitlyPresent2() {
        Logger.info("start test");
        Browser.get().open(PLNKR);
        Browser.get().configImplicitWait(0);
        Browser.get().waitForPresent(By.id("dynamic-paragraph"));
        Logger.info("finish test");
    }

    @Test(description = "conflict with implicit timeout")
    public void waitForElementExplicitlyPresentFailed() {
        Logger.info("start test 2");
        Browser.get().open(PLNKR);
        Browser.get().takeScreenshot();
        Browser.get().configImplicitWait(2);
        Browser.get().waitForPresent(By.id("dynamic-paragraph2"));
        Logger.info("finish test 2");
    }

    @Test(description = "timeout too long")
    public void waitForElementExplicitlyTooLong() {
        Logger.info("start test");
        Browser.get().open(PLNKR);
        Browser.get().configImplicitWait(20);
        Browser.get().waitForPresent(By.id("dynamic-paragraph"));
        Logger.info("finish test");
    }
}
