package additionalClasses;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Natashka on 18.03.2015.
 */
public class RandomText {

    private String subject;
    private String content;

    public String randomStringSubject (int symbol){
        subject = "Test subject: " + RandomStringUtils.randomAlphabetic(symbol);
        return subject;
    }
    public String randomStringContent (int symbol){
        content = "Test content: " + RandomStringUtils.randomAlphabetic(symbol);
        return content;
    }
}
