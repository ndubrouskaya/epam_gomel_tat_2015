package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 13.03.2015.
 */
public class UnsuccessLoginYandexMailTest {

    public static final String BASE_URL = "http://www.yandex.ru";

    //UI data
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector("button.user__enter");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg']");

    //Error message data
    public static final String errorMessageText = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    // Account data
    private String userLogin = "natashka.test";
    private String userPassword = "kiki";

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }
    @Test(description = "Success login to Yandex Mail")
    public void unSuccessLogin(){
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPassword);
        passwdInput.submit();
        WebElement messageText = driver.findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertEquals(messageText.getText(), errorMessageText, "Text of error message is wrong!");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}
