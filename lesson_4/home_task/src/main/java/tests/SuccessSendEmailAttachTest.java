package tests;

import additionalClasses.RandomText;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 14.03.2015.
 */
public class SuccessSendEmailAttachTest {
    public static final String BASE_URL = "http://www.yandex.ru";

    //UI data
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector("button.user__enter");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_FIELD_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_FIELD_LOCATOR = By.name("subj");
    public static final By MAIL_TEXTAREA_LOCATOR = By.id("compose-send");
    public static final By ATTACH_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By ATTACHED_FILE_LINK_LOCATOR = By.xpath("//div[@data-action='messages.show-attachments']");
    public static final By DOWNLOAD_FILE_BUTTON_LOCATOR = By.xpath("//*[@class='b-popup__box__content']//button//*[text()='Скачать файл']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";


    // Account data
    private String userLogin = "natashka.test";
    private String userPassword = "ntcnjdsq";
    private String mailTo = "ndubrouskaya@yandex.ru";
    private String mailSubject = new RandomText().randomStringSubject(10);
    private String mailContent = new RandomText().randomStringContent(100);

    //File data
    File attachedFile = new File("D:\\attached_file.txt");
    private String filePath = attachedFile.getAbsolutePath();


    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;


    @BeforeClass(description = "Write random content to file attached_file.txt")
    public void writeContentToFile() throws IOException {
        String contentAddFile = "Random content: " + RandomStringUtils.randomAlphabetic(100);
        FileUtils.writeStringToFile(attachedFile, contentAddFile);
    }

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser(){
        FirefoxProfile fxProfile = new FirefoxProfile();
        fxProfile.setPreference("browser.download.folderList",2);
        fxProfile.setPreference("browser.download.manager.showWhenStarting",false);
        fxProfile.setPreference("browser.download.dir","\\");
        fxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","text/plain");

        driver = new FirefoxDriver(fxProfile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test(description = "Success login")
    public void successLogin(){
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPassword);
        passwdInput.submit();
    }

    @Test(description = "Success send email with attachment", dependsOnMethods = "successLogin")
    public void successSendFile() throws InterruptedException, IOException {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_FIELD_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_FIELD_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXTAREA_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement attachFileButton = waitForElement(ATTACH_BUTTON_LOCATOR);
        attachFileButton.sendKeys(filePath);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        Thread.sleep(2000);
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));
        WebElement attachedFileLink = driver.findElement(ATTACHED_FILE_LINK_LOCATOR);
        attachedFileLink.click();
        WebElement downloadButton = driver.findElement(DOWNLOAD_FILE_BUTTON_LOCATOR);
        downloadButton.click();
        Thread.sleep(2000);
        File downloadFile = new File("\\"+attachedFile.getName());
        boolean result = FileUtils.contentEquals(attachedFile, downloadFile);
        Assert.assertEquals(true, result, "Files are different!");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
