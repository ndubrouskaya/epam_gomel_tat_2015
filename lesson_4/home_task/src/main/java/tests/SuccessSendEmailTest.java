package tests;

import additionalClasses.RandomText;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SuccessSendEmailTest{

    public static final String BASE_URL = "http://www.yandex.ru";

    //UI data
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector("button.user__enter");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_FIELD_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_FIELD_LOCATOR = By.name("subj");
    public static final By MAIL_TEXTAREA_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By MESSAGE_LOCATOR = By.xpath("//div[@class='block-compose-done']//div[@class='b-done-title']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final String SENT_SUBJECT_LOCATOR_PATTERN = "//label[text()='Отправленные']//ancestor::div//span[@class='b-messages__subject'][text()='%s']";

    //Messages data
    public static final String successSentMessage = "Письмо успешно отправлено.";

    // Account data
    private String userLogin = "natashka.test";
    private String userPassword = "ntcnjdsq";

    // Data for writing email
    private String mailTo = "natashka.test@yandex.ru";
    private String mailSubject = new RandomText().randomStringSubject(10);
    private String mailContent = new RandomText().randomStringContent(100);

    // Tools data
    public static final int TIME_OUT_MAIL_SENT_SECONDS = 20;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success login")
    public void successLogin(){
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPassword);
        passwdInput.submit();
    }


    @Test(description = "Success send email", dependsOnMethods = {"successLogin"})
    public void successSendEmail(){
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_FIELD_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_FIELD_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXTAREA_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        WebElement messageText = driver.findElement(MESSAGE_LOCATOR);
        Assert.assertEquals(messageText.getText(), successSentMessage, "No message 'Письмо успешно отправлено.'");
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_SENT_SECONDS).until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format(SENT_SUBJECT_LOCATOR_PATTERN, mailSubject))));
    }


    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }


}
