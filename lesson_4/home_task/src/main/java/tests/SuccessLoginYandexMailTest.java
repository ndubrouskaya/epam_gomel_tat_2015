package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 13.03.2015.
 */
public class SuccessLoginYandexMailTest {

    public static final String BASE_URL = "http://www.yandex.ru";

    //UI data
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector("button.user__enter");
    public static final  By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final String ACCOUNT_LINK_LOCATOR_PATTERN = "//a[@id='nb-1']//span[text()='%s@yandex.ru']";

    // Account data
    private String userLogin = "natashka.test";
    private String userPassword = "ntcnjdsq";

    // Tools data
    public static final int ACCOUNT_LOAD_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }
    @Test(description = "Success login to Yandex Mail")
    public void successLogin(){
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPassword);
        passwdInput.submit();
        new WebDriverWait(driver, ACCOUNT_LOAD_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(ACCOUNT_LINK_LOCATOR_PATTERN, userLogin))));
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }



}
