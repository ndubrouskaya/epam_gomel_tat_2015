package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.mail.Letter;
import com.epam.gomel.tat.lesson_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 20.03.2015.
 */
public class SendMailTest extends LoginBaseTest {

    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getLetter();

    @Test(description = "Success send mail")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent(){
        mailGuiService.checkMailInSent(letter);
    }
}
