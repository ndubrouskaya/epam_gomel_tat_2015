package com.epam.gomel.tat.lesson_6.bo.common;


/**
 * Created by Natashka on 22.03.2015.
 */
public class AccountBuilder {

    public static String userLogin = "natashka.test";
    public static String userPassword = "ntcnjdsq";
    public static String userEmail =  "natashka.test@yandex.ru";
    public static String userPasswordIncorrect = "dgsdsg";

    public static Account getDefaultAccount() {
        Account account = new Account(userLogin, userPassword, userEmail);
        return account;
    }
    public static Account getDefaultAccountWrongPassword() {
        Account account = new Account(userLogin, userPasswordIncorrect, userEmail);
        return account;
    }
}
