package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.mail.Letter;
import com.epam.gomel.tat.lesson_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 20.03.2015.
 */
public class DeleteMailTest extends LoginBaseTest {

    private Letter letter = LetterBuilder.getLetter();
    private MailGuiService mailGuiService = new MailGuiService();

    @Test(description = "Success send mail")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent(){
        mailGuiService.checkMailInSent(letter);
    }

    @Test(description = "Delete new mail from inbox", dependsOnMethods = {"checkMailInSent"})
    public void deleteMail() {
        mailGuiService.deleteMail(letter);
    }

    @Test(description = "Checking mail in deleted list", dependsOnMethods = {"deleteMail"})
    public void checkMailInDeleted(){
        mailGuiService.checkMailInDeleted(letter);
    }
}
