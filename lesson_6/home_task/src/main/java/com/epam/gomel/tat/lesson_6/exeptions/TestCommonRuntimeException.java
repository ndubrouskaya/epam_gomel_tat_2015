package com.epam.gomel.tat.lesson_6.exeptions;

/**
 * Created by Natashka on 22.03.2015.
 */
public class TestCommonRuntimeException extends RuntimeException {

    public TestCommonRuntimeException(String message) {
        super(message);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
