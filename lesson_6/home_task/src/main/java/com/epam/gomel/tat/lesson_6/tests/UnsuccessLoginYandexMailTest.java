package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_6.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 20.03.2015.
 */
public class UnsuccessLoginYandexMailTest {

   private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccountIncorrect;

    @Test(description = "Unsuccess login to Yandex Mail")
    public void unSuccessLogin(){
        defaultAccountIncorrect = AccountBuilder.getDefaultAccountWrongPassword();
        loginGuiService.loginToAccountError(defaultAccountIncorrect);
    }
}
