package com.epam.gomel.tat.lesson_6.bo.mail;

import com.epam.gomel.tat.lesson_6.utils.MyFileUtils;
import com.epam.gomel.tat.lesson_6.utils.RandomText;

import java.io.File;

/**
 * Created by Natashka on 22.03.2015.
 */
public class LetterBuilder {
    public static String receiver = "natashka.test@yandex.ru";

    public static Letter getLetter(){
        Letter letter = new Letter(receiver,RandomText.randomString(10),RandomText.randomString(100));
        return letter;
    }

    public static Letter getLetterAttach(){
        return new Letter(receiver,RandomText.randomString(10),RandomText.randomString(100), MyFileUtils.createNewFile(new File("D:\\"+RandomText.randomString(5)+".txt")).getAbsolutePath());
    }

}
