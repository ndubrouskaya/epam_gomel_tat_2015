package com.epam.gomel.tat.lesson_6.service;

import com.epam.gomel.tat.lesson_6.bo.mail.Letter;
import com.epam.gomel.tat.lesson_6.exeptions.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.pages.*;
import com.epam.gomel.tat.lesson_6.reporting.Logger;
import com.epam.gomel.tat.lesson_6.ui.Browser;
import com.epam.gomel.tat.lesson_6.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Natashka on 22.03.2015.
 */
public class MailGuiService {

    public void sendMail(Letter letter) {
        Logger.info("Send mail to " + letter.getReceiver() + " with subject " + letter.getSubject());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().openComposeMail().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttachment());
    }

    public void checkMailInSent(Letter letter) {
        Logger.info("Check if sent mail with subject " + letter.getSubject() + " present in sent list");
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterContent = mailbox.openSentListPage().openLetter(letter.getSubject());
        String subject = letterContent.getLetterSubject();
        String content = letterContent.getLetterContent();
        if (subject == null || !subject.equals(letter.getSubject())) {
            throw new TestCommonRuntimeException(subject + " - is incorrect! Subject of the sent mail: '" + letter.getSubject() + "'");
        }
        if (content == null || !content.equals(letter.getContent())) {
            throw new TestCommonRuntimeException("Content of sent mail is incorrect!");
        }
        if (letter.getAttachment() != null) {
            Logger.info("Download attached file");
            letterContent.downloadAttachedFile();
            File attachedFile = new File(letter.getAttachment());
            File downloadFile = new File(Browser.DOWNLOAD_DIR + attachedFile.getName());
            MyFileUtils.waitForDownload(downloadFile);
            try {
                if (!FileUtils.contentEquals(new File(letter.getAttachment()), downloadFile)) {
                    Logger.error("Files different!");
                    throw new TestCommonRuntimeException("Files different!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void markAsSpam(Letter letter) {
        Logger.info("Mark mail with subject " + letter.getSubject() + " as spam");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().markMailAsSpam(letter.getSubject());
    }

    public void checkMailInSpam(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in spam list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailSpamListPage mailSpam = mailbox.openSpamListPage();
        if (mailSpam.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in spam!");
            throw new TestCommonRuntimeException("No message in spam!");
        }
    }

    public void deleteMail(Letter letter) {
        Logger.info("Delete mail with subject " + letter.getSubject());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().deleteCheckedMail(letter.getSubject());
    }

    public void checkMailInDeleted(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in deleted list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailDeletedListPage mailDeleted = mailbox.openDeletedListPage();
        if (mailDeleted.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in deleted!");
            throw new TestCommonRuntimeException("No message in deleted!");
        }
    }

    public void markAsNotSpam(Letter letter) {
        Logger.info("Mark mail with subject " + letter.getSubject() + " as not spam");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openSpamListPage().markMailNotSpam(letter.getSubject());
    }

    public void checkMailInInbox(Letter letter) {
        Logger.info("Check mail with subject " + letter.getSubject() + " in inbox list");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailInboxListPage mailInbox = mailbox.openInboxListPage();
        if (mailInbox.isMailPresent(letter.getSubject()) == false) {
            Logger.error("No message in inbox!");
            throw new TestCommonRuntimeException("No message in inbox!");
        }
    }
}
