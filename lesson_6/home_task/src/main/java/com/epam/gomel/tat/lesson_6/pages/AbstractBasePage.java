package com.epam.gomel.tat.lesson_6.pages;

import com.epam.gomel.tat.lesson_6.ui.Browser;

/**
 * Created by Natashka on 20.03.2015.
 */
public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage(){
        this.browser = Browser.get();
    }


}
