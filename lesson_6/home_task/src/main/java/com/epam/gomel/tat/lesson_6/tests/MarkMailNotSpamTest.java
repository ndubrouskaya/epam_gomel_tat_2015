package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.mail.Letter;
import com.epam.gomel.tat.lesson_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 21.03.2015.
 */
public class MarkMailNotSpamTest extends LoginBaseTest {

    private Letter letter = LetterBuilder.getLetter();
    private MailGuiService mailGuiService = new MailGuiService();

    @Test(description = "Success send mail")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent() {
        mailGuiService.checkMailInSent(letter);
    }

    @Test(description = "Mark new mail as spam", dependsOnMethods = {"checkMailInSent"})
    public void markAsSpam() {
        mailGuiService.markAsSpam(letter);
    }

    @Test(description = "Checking mail in spam list", dependsOnMethods = {"markAsSpam"})
    public void checkMailInSpam() {
        mailGuiService.checkMailInSpam(letter);
    }

    @Test(description = "Mark mail as not spam", dependsOnMethods = {"checkMailInSpam"})
    public void markAsNotSpam() {
        mailGuiService.markAsNotSpam(letter);
    }

    @Test(description = "Checking spam mail in inbox", dependsOnMethods = {"markAsNotSpam"})
    public void checkMailInInbox() {
        mailGuiService.checkMailInInbox(letter);
    }
}
