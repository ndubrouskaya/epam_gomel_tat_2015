package com.epam.gomel.tat.lesson_6.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Natashka on 20.03.2015.
 */
public class RandomText {

    public static String randomString(int symbol){
        return RandomStringUtils.randomAlphabetic(symbol);
    }
}
