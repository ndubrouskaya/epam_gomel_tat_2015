package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_6.service.LoginGuiService;
import org.testng.annotations.BeforeClass;

/**
 * Created by Natashka on 23.03.2015.
 */
public class LoginBaseTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account account = AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Success login to yandex mail")
    public void successLogin(){
        loginGuiService.loginToAccountMailbox(account);
    }

}
