package com.epam.gomel.tat.lesson_6.ui;

import com.epam.gomel.tat.lesson_6.reporting.Logger;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 19.03.2015.
 */
public class Browser {

    public static final String DOWNLOAD_DIR = "D:\\_Downloads\\";
    public static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    public static final int WAIT_ELEMENT_TIMEOUT = 20;
    public static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int AJAX_TIMEOUT = 20;
    private WebDriver driver;

    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get(){
        if(instance != null){
            return instance;
        }
        return instance = init();
    }

    private static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }
    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }
    public void open(String url) {
        Logger.info("Open URL: " + url);
        driver.get(url);
    }

    public void click(By locator) {
        Logger.info("Click " + locator);
        driver.findElement(locator).click();
    }

    public void type(By locator, String text) {
        Logger.info("Type " + text + " into " + locator);
        driver.findElement(locator).sendKeys(text);
    }

    public void attachFile(By locator, String text) {
        Logger.info("Attach file " + text + " into " + locator);
        driver.findElement(locator).sendKeys(text);
    }

    public void submit(By locator) {
        Logger.info("Submit " + locator);
        driver.findElement(locator).submit();
    }

    public String getText(By locator){
        Logger.info("getText from " + locator);
        return driver.findElement(locator).getText();
    }

    public boolean isPresent(By locator) {
        Logger.info("Checking if element " + locator + " present");
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info("Wait for visible " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForNotVisible(By locator) {
        Logger.info("Wait for not visible " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed(){
        Logger.info("Wait for Ajax reguests");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean)((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public static void kill() {
        Logger.info("Close browser");
        if(instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instance = null;
            }
        }
    }


}

