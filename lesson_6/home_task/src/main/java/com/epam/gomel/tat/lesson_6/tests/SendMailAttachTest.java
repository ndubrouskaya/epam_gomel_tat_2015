package com.epam.gomel.tat.lesson_6.tests;

import com.epam.gomel.tat.lesson_6.bo.mail.Letter;
import com.epam.gomel.tat.lesson_6.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Natashka on 20.03.2015.
 */
public class SendMailAttachTest extends LoginBaseTest {

    private Letter letter = LetterBuilder.getLetterAttach();
    private MailGuiService mailGuiService = new MailGuiService();

    @Test(description = "Success send mail with attachment")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent() throws IOException {
        mailGuiService.checkMailInSent(letter);
    }
}
