package com.epam.gomel.tat.lesson_6.runner;

import com.epam.gomel.tat.lesson_6.reporting.CustomTestNGListener;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natashka on 23.03.2015.
 */
public class Runner {

    public static void main (String[] args){
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        testNG.addListener(new CustomTestNGListener());
        suites.add("target/classes/suites/"+args[0]);
        suites.add("target/classes/suites/"+args[1]);
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
