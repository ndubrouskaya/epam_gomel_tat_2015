package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 21.03.2015.
 */
public class LetterContentPage extends AbstractBasePage {

    public static final By LETTER_SUBJECT_LOCATOR = By.xpath("//div[contains(@class,' b-message_loaded')]//span[contains(@class,'js-message-subject')]");
    public static final By LETTER_CONTENT_LOCATOR = By.xpath("//div[contains(@class,' b-message_loaded')]//div[@class='b-message-body__content']");
    public static final By DOWNLOAD_LINK_LOCATOR = By.xpath("//div[contains(@class, 'b-message-attachments__common')]//a[contains(@class,'b-file__download')]");


    public String getLetterSubject() {
        return browser.getText(LETTER_SUBJECT_LOCATOR);
    }

    public String getLetterContent() {
        return browser.getText(LETTER_CONTENT_LOCATOR);
    }

    public void downloadAttachedFile(){
        browser.click(DOWNLOAD_LINK_LOCATOR);
    }
}
