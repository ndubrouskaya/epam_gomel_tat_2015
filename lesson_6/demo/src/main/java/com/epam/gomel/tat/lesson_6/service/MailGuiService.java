package com.epam.gomel.tat.lesson_6.service;

import com.epam.gomel.tat.lesson_6.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_6.pages.MailboxBasePage;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
    }

    public void checkMailInInbox(MailLetter letter) {
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().openLetter(letter.getSubject());
        // TODO ...
    }

    public void checkMailInInboxList(MailLetter letter) {
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().openLetter(letter.getSubject());
        // TODO ...
    }


    public void checkMailInSpam(MailLetter letter) {
        // TODO ...
    }

}
