package com.epam.gomel.tat.lesson_6.bo.common;

public enum DefaultAccounts {
    DEFAULT_MAIL("tat-test-user", "tat-123qwe", "tat-test-user@yandex.ru");

    private Account account;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}
