package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

public class MailSentListPage extends AbstractPage {
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

}
