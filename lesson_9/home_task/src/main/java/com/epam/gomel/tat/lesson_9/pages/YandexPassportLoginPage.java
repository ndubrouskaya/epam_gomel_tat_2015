package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 02.04.2015.
 */
public class YandexPassportLoginPage extends AbstractBasePage {

    public static final String YANDEX_DISK_URL = "https://passport.yandex.ru";
    public static final By LOGIN_INPUT_LOCATOR = By.id("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.id("passwd");
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//div[@class='domik-submit-button']/button");

    public YandexPassportLoginPage open(){
        browser.open(YANDEX_DISK_URL);
        return this;
    }

    public YandexPassportPage login(String login, String password){
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.submit(ENTER_BUTTON_LOCATOR);
        return new YandexPassportPage();
    }
}
