package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 21.03.2015.
 */
public class LetterContentPage extends AbstractBasePage {
    public static final By LETTER_RECEIVER_LOCATOR = By.xpath("//div[contains(@class,'b-message-head__field__content')][text()='Получатель: ']//span[@class ='b-message-head__name']");
    public static final By LETTER_RECEIVER_EMAIL_LOCATOR = By.xpath("//div[@class ='b-mail-dropdown__box']//span[@class='b-mail-dropdown__item__content']");
    public static final By LETTER_SUBJECT_LOCATOR = By.xpath("//div[contains(@class,' b-message_loaded')]//span[contains(@class,'js-message-subject')]");
    public static final By LETTER_CONTENT_LOCATOR = By.xpath("//div[contains(@class,' b-message_loaded')]//div[@class='b-message-body__content']");
    public static final By DOWNLOAD_LINK_LOCATOR = By.xpath("//div[contains(@class, 'b-message-attachments__common')]//a[contains(@class,'b-file__download')]");


    public String getLetterReceiver() {
        browser.waitForAjaxProcessed();
        browser.click(LETTER_RECEIVER_LOCATOR);
        return browser.getText(LETTER_RECEIVER_EMAIL_LOCATOR);
    }

    public String getLetterSubject() {
        return browser.getText(LETTER_SUBJECT_LOCATOR);
    }

    public String getLetterContent() {
        return browser.getText(LETTER_CONTENT_LOCATOR);
    }

    public void downloadAttachedFile() {
        browser.click(DOWNLOAD_LINK_LOCATOR);
    }
}
