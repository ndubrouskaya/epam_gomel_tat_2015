package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.GlobalConfig;
import com.epam.gomel.tat.lesson_9.exeptions.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_9.pages.YandexDiskPage;
import com.epam.gomel.tat.lesson_9.pages.YandexDiskTrashPage;
import com.epam.gomel.tat.lesson_9.reporting.Logger;
import com.epam.gomel.tat.lesson_9.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Natashka on 03.04.2015.
 */
public class YandexDiskGuiService {

    public void uploadFile(File file) {
        YandexDiskPage yandexDisk = new YandexDiskPage().uploadFile(file.getAbsolutePath());
        if (yandexDisk.isFilePresent(file.getName()) == false) {
            Logger.error("No uploaded file with title " + file.getName() + " on the  yandexDisk!");
            throw new TestCommonRuntimeException("No uploaded file on the disk!");
        }
        yandexDisk.downloadFile(file.getName());
        File uploadFile = new File(file.getAbsolutePath());
        File downloadFile = new File(GlobalConfig.DOWNLOAD_DIR + uploadFile.getName());
        MyFileUtils.waitForDownload(downloadFile);
        try {
            if (!FileUtils.contentEquals(new File(file.getAbsolutePath()), downloadFile)) {
                Logger.error("Files different!");
                new TestCommonRuntimeException("Downloaded file and uploaded files are different!");
            }
        } catch (IOException e) {
            Logger.error("Can't compare files!",e);
        }
    }

    public void dragAndDropFileIntoTrash(File file) {
        YandexDiskPage yandexDisk = new YandexDiskPage().dragAndDrop(file.getName());
        YandexDiskTrashPage yandexDiskTrash = yandexDisk.openTrash();
        if(yandexDiskTrash.isFilePresent(file.getName()) == false){
            Logger.error("No deleted file with title " + file.getName() + " in the trash!");
            throw new TestCommonRuntimeException("No deleted file in the trash!");
        }
    }

    public void recoverFileFromTrash(File file) {
        YandexDiskTrashPage yandexDiskTrash = new YandexDiskTrashPage().recoverFile(file.getName());
        YandexDiskPage yandexDisk = yandexDiskTrash.openYandexDisk();
        if (yandexDisk.isFilePresent(file.getName()) == false) {
            Logger.error("No recovered file with title " + file.getName() + " on the  yandexDisk!");
            throw new TestCommonRuntimeException("No recovered file on the disk!");
        }
    }
}
