package com.epam.gomel.tat.lesson_9.tests;

import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import org.testng.annotations.Listeners;

/**
 * Created by Natashka on 08.04.2015.
 */

@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
        MethodListener.class })

public class BaseTest {

}
