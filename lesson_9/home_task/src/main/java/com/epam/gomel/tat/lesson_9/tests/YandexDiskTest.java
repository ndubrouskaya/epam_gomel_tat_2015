package com.epam.gomel.tat.lesson_9.tests;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.bo.file.FileBuilder;
import com.epam.gomel.tat.lesson_9.service.YandexDiskGuiService;
import com.epam.gomel.tat.lesson_9.service.YandexPassportGuiService;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by Natashka on 02.04.2015.
 */
public class YandexDiskTest extends BaseTest{

    private Account defaultAccount = AccountBuilder.getDefaultAccount();;
    private File file = FileBuilder.getFileToUpload();
    YandexPassportGuiService yandexPassport = new YandexPassportGuiService();
    YandexDiskGuiService yandexDisk = new YandexDiskGuiService();

    @Test(description = "Login to YandexDisk")
    public void loginYandexDisk(){
        yandexPassport.loginToYandexPassport(defaultAccount);
        yandexPassport.goToYandexDisk();
    }

    @Test(description = "Upload file to YandexDisk", dependsOnMethods = {"loginYandexDisk"})
    public void uploadFile(){
        yandexDisk.uploadFile(file);
    }

    @Test(description = "Drag and drop file to trash", dependsOnMethods = {"uploadFile"})
    public void moveFileIntoTrash(){
        yandexDisk.dragAndDropFileIntoTrash(file);
    }

    @Test(description = "Recycle file from trash", dependsOnMethods = {"moveFileIntoTrash"})
    public void fileRecovery(){
        yandexDisk.recoverFileFromTrash(file);
    }

}
