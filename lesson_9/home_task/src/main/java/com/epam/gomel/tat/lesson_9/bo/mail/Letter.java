package com.epam.gomel.tat.lesson_9.bo.mail;

/**
 * Created by Natashka on 22.03.2015.
 */
public class Letter {

    private String receiver;
    private String subject;
    private String content;
    private String attachment;

    public Letter(String receiver, String subject, String content) {
        this.setReceiver(receiver);
        this.setSubject(subject);
        this.setContent(content);
    }

    public Letter(String receiver, String subject, String content, String attachment) {
        this.setReceiver(receiver);
        this.setSubject(subject);
        this.setContent(content);
        this.setAttachment(attachment);
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
