package com.epam.gomel.tat.lesson_9.utils;

import com.epam.gomel.tat.lesson_9.ui.Browser;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 24.03.2015.
 */
public class MyFileUtils {


    public static File createNewFile(File file){
        try {
            FileUtils.writeStringToFile(file, RandomText.randomStringLong());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void waitForDownload(File file) {
        new FluentWait<File>(file).withTimeout(Browser.WAIT_ELEMENT_TIMEOUT, TimeUnit.SECONDS).until(new Predicate<File>() {
            @Override
            public boolean apply(File file) {
                return file.exists();
            }
        });
    }
}
