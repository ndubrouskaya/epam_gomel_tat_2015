package com.epam.gomel.tat.lesson_9.bo.file;

import com.epam.gomel.tat.lesson_9.utils.MyFileUtils;
import com.epam.gomel.tat.lesson_9.utils.RandomText;

import java.io.File;

/**
 * Created by Natashka on 03.04.2015.
 */
public class FileBuilder {

    public static File getFileToUpload(){
        File file = new File(MyFileUtils.createNewFile(new File(RandomText.randomStringShort() + ".txt")).getAbsolutePath());
        return file;
    }
}
