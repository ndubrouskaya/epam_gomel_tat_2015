package com.epam.gomel.tat.lesson_9.bo.common;


/**
 * Created by Natashka on 22.03.2015.
 */
public class AccountBuilder {

    public static String USER_LOGIN = "natashka.test";
    public static String USER_PASSWORD = "ntcnjdsq";
    public static String USER_EMAIL =  "natashka.test@yandex.ru";
    public static String USER_PASSWORD_INCORRECT = "dgsdsg";

    public static Account getDefaultAccount() {
        Account account = new Account(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        return account;
    }
    public static Account getDefaultAccountWrongPassword() {
        Account account = getDefaultAccount();
        account.setPassword(USER_PASSWORD_INCORRECT);
        return account;
    }
}
