package com.epam.gomel.tat.lesson_9.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Natashka on 20.03.2015.
 */
public class RandomText {

    public static String randomStringLong(){
        return RandomStringUtils.randomAlphabetic(100);
    }

    public static String randomStringShort(){
        return RandomStringUtils.randomAlphabetic(5);
    }
}
