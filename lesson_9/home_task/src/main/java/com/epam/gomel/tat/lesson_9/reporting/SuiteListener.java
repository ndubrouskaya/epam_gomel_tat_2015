package com.epam.gomel.tat.lesson_9.reporting;

import com.epam.gomel.tat.lesson_9.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by Natashka on 06.04.2015.
 */
public class SuiteListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode().getAlias());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
        Logger.info("Set parameters to suite " + suite.getName());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
