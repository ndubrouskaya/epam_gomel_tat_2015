package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 02.04.2015.
 */
public class YandexPassportPage extends AbstractBasePage {
    
    private static final By LOGIN_DATA_LOCATOR = By.xpath("//div[@class='b-profile__wrap']//b[@class='b-user']");
    private static final By ELSE_LINK_LOCATOR = By.xpath("//td[contains (@class,'b-head-tabs__more')]//span[@class='b-link__inner']");
    private static final By DISK_LINK_LOCATOR = By.xpath("//a[@href='http://disk.yandex.ru']") ;

    public String getUserLogin() {
        browser.waitForPresent(LOGIN_DATA_LOCATOR);
        return browser.getText(LOGIN_DATA_LOCATOR);
    }

    public YandexDiskPage goToDisk() {
        browser.click(ELSE_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        browser.click(DISK_LINK_LOCATOR);
        return new YandexDiskPage();
    }
}
