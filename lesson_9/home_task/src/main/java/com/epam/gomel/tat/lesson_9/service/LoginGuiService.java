package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.exeptions.TestLoginException;
import com.epam.gomel.tat.lesson_9.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_9.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_9.pages.MailLoginErrorPage;
import com.epam.gomel.tat.lesson_9.reporting.Logger;

/**
 * Created by Natashka on 22.03.2015.
 */
public class LoginGuiService {

    public void loginToAccountMailbox(Account account){
        Logger.info("Login to account " + account.getEmail());
        MailLoginPage mailLogin = new MailLoginPage().open();
        MailboxBasePage mailbox = mailLogin.login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())){
            Logger.error(account.getEmail() + " is incorrect!");
            throw new TestLoginException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void loginToAccountError(Account account) {
        Logger.info("Login to account " + account.getEmail() + " with incorrect password");
        MailLoginPage mailLogin = new MailLoginPage().open();
        MailLoginErrorPage yandexPassport = mailLogin.unsuccessLogin(account.getLogin(), account.getPassword());
        String errorMessage = yandexPassport.getErrorMessage();
        if (errorMessage == null || !errorMessage.equals(MailLoginErrorPage.ERROR_MESSAGE_TEXT)){
            throw new TestLoginException("Wrong error message!");
        }

    }
}
