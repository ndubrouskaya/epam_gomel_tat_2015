package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 04.04.2015.
 */
public class YandexDiskTrashPage extends AbstractBasePage{

    private static final String FILE_LOCATOR_PATTERN = "//div[contains(@class,'ns-view-listing')][contains(@data-key,'trash')]//div[contains(@class,'nb-resource')][contains(@title,'%s')]";
    private static final String RECOVER_BUTTON_LOCATOR_PATTERN = "//div[contains(@class,'nb-panel__title')]//ancestor::div[contains(@class,'ns-view-aside')][contains(@data-key,'%s')]//button[contains(@class,'nb-button')][@data-click-action='resource.restore']";
    private static final By YANDEX_DISK_ICON_LOCATOR = By.xpath("//a[contains(@class,'b-crumbs__root')]");

    public boolean isFilePresent(String fileTitle) {
        return browser.isPresent(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileTitle)));
    }

    public YandexDiskTrashPage recoverFile(String fileTitle) {
        browser.waitForVisible(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileTitle)));
        browser.click(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileTitle)));
        browser.waitForVisible(By.xpath(String.format(RECOVER_BUTTON_LOCATOR_PATTERN,fileTitle)));
        browser.click(By.xpath(String.format(RECOVER_BUTTON_LOCATOR_PATTERN,fileTitle)));
        browser.waitForAjaxProcessed();
        return new YandexDiskTrashPage();
    }

    public YandexDiskPage openYandexDisk() {
        browser.click(YANDEX_DISK_ICON_LOCATOR);
        return new YandexDiskPage();
    }
}
