package com.epam.gomel.tat.lesson_9.tests;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 19.03.2015.
 */
public class SuccessLoginYandexMailTest extends BaseTest{

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount;

    @Test(description = "Success login to Yandex mail account")
    public void successLogin() {
        defaultAccount = AccountBuilder.getDefaultAccount();
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }
}

