package com.epam.gomel.tat.lesson_9.bo.common;

/**
 * Created by Natashka on 22.03.2015.
 */
public class Account {
    private String login;
    private String password;
    private String email;

    public Account(String login, String password, String email){
        this.setLogin(login);
        this.setPassword(password);
        this.setEmail(email);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
