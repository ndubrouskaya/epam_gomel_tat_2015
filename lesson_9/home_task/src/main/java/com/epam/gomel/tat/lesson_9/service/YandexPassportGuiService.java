package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.exeptions.TestLoginException;
import com.epam.gomel.tat.lesson_9.pages.YandexDiskPage;
import com.epam.gomel.tat.lesson_9.pages.YandexPassportLoginPage;
import com.epam.gomel.tat.lesson_9.pages.YandexPassportPage;
import com.epam.gomel.tat.lesson_9.reporting.Logger;

/**
 * Created by Natashka on 02.04.2015.
 */
public class YandexPassportGuiService {

    public void loginToYandexPassport(Account account){
        YandexPassportLoginPage yandexLogin = new YandexPassportLoginPage().open();
        YandexPassportPage yandexPassport = yandexLogin.login(account.getLogin(), account.getPassword());
        String userLogin = yandexPassport.getUserLogin();
        if (userLogin == null || !userLogin.equals(account.getLogin())){
            Logger.error("Login " + account.getLogin() + " is incorrect!");
            throw new TestLoginException("Login to YandexPassport failed!");
        }
    }

    public void goToYandexDisk(){
        YandexDiskPage yandexDisk = new YandexPassportPage().goToDisk();
        String userLogin = yandexDisk.getUserLogin();
        if (userLogin == null || !userLogin.equals(AccountBuilder.USER_LOGIN)){
            Logger.error("Login is incorrect!");
            throw new TestLoginException("Login to YandexDisk failed!");
        }
    }
}
