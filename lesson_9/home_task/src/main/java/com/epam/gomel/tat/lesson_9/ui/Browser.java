package com.epam.gomel.tat.lesson_9.ui;

import atu.testng.reports.ATUReports;
import com.epam.gomel.tat.lesson_9.GlobalConfig;
import com.epam.gomel.tat.lesson_9.reporting.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 19.03.2015.
 */
public class Browser {

    public static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt, image/jpeg";
    public static final int WAIT_ELEMENT_TIMEOUT = 20;
    public static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int AJAX_TIMEOUT = 20;
    public static final String CHROME_DRIVER_PATH = "./chromedriver.exe";
    public static final String URL_PATTERN = "http://%s:%d/wd/hub";
    private static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private WebDriver driver;
    private boolean augmented;

    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = new Thread().currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    public static void kill() {
        Logger.info("Close browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    private static Browser init() {
        BrowserType browserType = GlobalConfig.getInstance().getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                try {
                    Logger.info("RemoteWebdriver parameters: host: " + GlobalConfig.getInstance().getHost() + " and port: " + GlobalConfig.getInstance().getPort());
                    driver = new RemoteWebDriver(new URL(String.format(URL_PATTERN, GlobalConfig.getInstance().getHost(), GlobalConfig.getInstance().getPort())),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    Logger.error("URL is invalid!", e);
                    throw new RuntimeException("Invalid url format");
                }
                driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
                driver = new ChromeDriver(getChromeDriverProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
        }
        ATUReports.setWebDriver(driver);
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalConfig.DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    private static DesiredCapabilities getChromeDriverProfile() {
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", GlobalConfig.DOWNLOAD_DIR);
        chromePrefs.put("download.prompt_for_download", false);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    public void takeScreenshot() {
        if (GlobalConfig.getInstance().getBrowserType() == BrowserType.REMOTE && !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
            FileUtils.copyFile(screenshot, copy);
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public void open(String url) {
        Logger.info("Open URL: " + url);
        driver.get(url);
        takeScreenshot();
    }

    public void click(By locator) {
        Logger.info("Click " + locator);
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        element.click();
    }

    public void type(By locator, String text) {
        Logger.info("Type " + text + " into " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public void attachFile(By locator, String text) {
        Logger.info("Attach file " + text + " into " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public void uploadFile(By locator, String text) {
        Logger.info("Load file " + text + " into " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public void submit(By locator) {
        Logger.info("Submit " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        element.submit();
    }

    public String getText(By locator) {
        Logger.info("GetText from " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        return element.getText();
    }

    public void closePresentDialog(By locator) {
        Logger.info("Close modal dialog.");
        driver.switchTo().activeElement();
        waitForPresent(locator);
        takeScreenshot();
        click(locator);
}

    public void dragAndDrop(By locator1, By locator2) {
        Logger.info("Element " + locator1.toString() + " drag and drop to element " + locator2.toString());
        waitForPresent(locator1);
        takeScreenshot();
        waitForPresent(locator2);
        takeScreenshot();
        WebElement element1 = driver.findElement(locator1);
        elementHighlight(element1);
        WebElement element2 = driver.findElement(locator2);
        elementHighlight(element2);
        new Actions(driver).dragAndDrop(element1, element2).build().perform();
    }

    public void doubleClick(By locator) {
        Logger.info("Double click on element " + locator.toString());
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        new Actions(driver).doubleClick(element).build().perform();
    }

    public boolean isPresent(By locator) {
        Logger.info("Checking if element " + locator.toString() + " present");
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present " + locator.toString());
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info("Wait for visible " + locator.toString());
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForNotVisible(By locator) {
        Logger.info("Wait for not visible " + locator.toString());
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info("Wait for Ajax reguests");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }




}

