package com.epam.gomel.tat.lesson_9.tests;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.bo.mail.Letter;
import com.epam.gomel.tat.lesson_9.bo.mail.LetterBuilder;
import com.epam.gomel.tat.lesson_9.service.LoginGuiService;
import com.epam.gomel.tat.lesson_9.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Natashka on 20.03.2015.
 */
public class DeleteMailTest extends BaseTest{

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account account = AccountBuilder.getDefaultAccount();
    private Letter letter = LetterBuilder.getLetter();
    private MailGuiService mailGuiService = new MailGuiService();

    @BeforeClass(description = "Success login to yandex mail")
    public void successLogin(){
        loginGuiService.loginToAccountMailbox(account);
    }

    @Test(description = "Success send mail")
    public void successSendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Checking mail in sent list", dependsOnMethods = {"successSendMail"})
    public void checkMailInSent(){
        mailGuiService.checkMailInSent(letter);
    }

    @Test(description = "Delete new mail from inbox", dependsOnMethods = {"checkMailInSent"})
    public void deleteMail() {
        mailGuiService.deleteMail(letter);
    }

    @Test(description = "Checking mail in deleted list", dependsOnMethods = {"deleteMail"})
    public void checkMailInDeleted(){
        mailGuiService.checkMailInDeleted(letter);
    }
}
