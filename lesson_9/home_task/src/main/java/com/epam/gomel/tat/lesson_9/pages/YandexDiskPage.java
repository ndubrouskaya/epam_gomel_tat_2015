package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

/**
 * Created by Natashka on 02.04.2015.
 */
public class YandexDiskPage extends AbstractBasePage {
    private static final By LOGIN_LINK_LOCATOR_PATTERN = By.xpath("//span[contains(@class,'_nb-user-name')]");
    private static final By CLOSE_UPLOAD_DIALOG_LOCATOR = By.xpath("//button[contains(@class,'b-dialog-upload__button-close')]");
    private static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//label[contains(@class,'nb-button')]//input[@type='file']");
    private static final String FILE_LOCATOR_PATTERN = "//div[contains(@class,'ns-view-listing')][contains(@data-key,'disk')]//div[contains(@class,'nb-resource')][contains(@title,'%s')]";
    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//div[contains(@class,'nb-panel__title__name')][contains(text(),'%s')]//ancestor::div[contains(@class,'nb-panel__content')]//button[@data-click-action='resource.download']";
    private static final By TRASH_BOX_LOCATOR = By.xpath("//div[contains(@class,'ns-view-listing')][contains(@data-key,'disk')]//div[@data-id='/trash']/div");
            //("//div[contains(@class,'nb-resource')][@title='Корзина']");
//            ("//div[contains(@class,'ns-view-listing')][contains(@data-key,'disk')]//div[contains(@class,'nb-resource')][@title='Корзина')]");

    public String getUserLogin() {
        browser.waitForPresent(LOGIN_LINK_LOCATOR_PATTERN);
        return browser.getText(LOGIN_LINK_LOCATOR_PATTERN);
    }

    public YandexDiskPage uploadFile(String file) {
        browser.uploadFile(UPLOAD_BUTTON_LOCATOR, file);
        browser.waitForVisible(CLOSE_UPLOAD_DIALOG_LOCATOR);
        browser.closePresentDialog(CLOSE_UPLOAD_DIALOG_LOCATOR);
        return new YandexDiskPage();
    }

    public boolean isFilePresent(String fileTitle) {
        return browser.isPresent(By.xpath(String.format(FILE_LOCATOR_PATTERN,fileTitle)));
    }

    public YandexDiskPage downloadFile(String file){
        browser.click(By.xpath(String.format(FILE_LOCATOR_PATTERN,file)));
        browser.click(By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN,file)));
        return new YandexDiskPage();
    }


    public YandexDiskPage dragAndDrop(String fileTitle) {
        browser.dragAndDrop(By.xpath(String.format(FILE_LOCATOR_PATTERN,fileTitle)), TRASH_BOX_LOCATOR);
//        browser.waitForNotVisible(By.xpath(String.format(FILE_LOCATOR_PATTERN,fileTitle)));
        browser.waitForAjaxProcessed();
        return new YandexDiskPage();
    }

    public YandexDiskTrashPage openTrash() {
        browser.doubleClick(TRASH_BOX_LOCATOR);
        browser.waitForAjaxProcessed();
        return new YandexDiskTrashPage();
    }
}
