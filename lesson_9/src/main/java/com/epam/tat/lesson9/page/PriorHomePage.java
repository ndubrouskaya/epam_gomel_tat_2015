package com.epam.tat.lesson9.page;

import com.epam.tat.lesson9.browser.Browser;
import com.epam.tat.lesson9.utils.Logger;
import org.openqa.selenium.By;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class PriorHomePage {

    private static final String PRIOR_HOME = "http://prior.by";
    private static By login = By.xpath("//*[@name='ctl00$MainContent$loginForm$tbName']");
    private static By password = By.xpath("//*[@name='ctl00$MainContent$loginForm$tbPassword']");
    private static By submit = By.id("MainContent_loginForm_rbLogin_Image");
    private static By antiRobot = By.id("MainContent_loginForm_rfvAntiRobotKeyword");

    public static PriorHomePage open() {
        Browser.get().open("http://prior.by");
        Browser.get().waitForPresent(login);
        return new PriorHomePage();
    }

    public PriorHomePage login(String user, String pass) {

        Browser.get().type(login, user);
        Browser.get().type(password, pass);
        Browser.get().click(submit);
        return this;
    }

    public void checkAntiRobotWorks() {
        Browser.get().waitForPresent(antiRobot);
        Logger.info("Got: " + Browser.get().getText(antiRobot));
    }
}
