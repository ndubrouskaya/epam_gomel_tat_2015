package com.epam.tat.lesson9.utils;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Timeout {

    public static void sleep(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            Logger.info("sleep interrupted");
        }
    }
}
