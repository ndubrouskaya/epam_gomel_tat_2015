package com.epam.tat.lesson9.test;

import com.epam.tat.lesson9.browser.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class BaseTest {

    @BeforeClass
    public void prepareBrowser() {
        Browser.get();
    }

    @AfterClass
    public void stopBrowser() {
        Browser.kill();
    }
}
