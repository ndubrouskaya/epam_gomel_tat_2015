package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.pages.MailLoginErrorPage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;

/**
 * Created by Natashka on 11.04.2015.
 */
public class LoginService {
    public void loginToMailbox(Account user) {
        Logger.info("Login as : " + user);
        MailLoginPage mailLogin = new MailLoginPage().open();
        mailLogin.login(user.getLogin(), user.getPassword());
    }

    public boolean isExpectedPagePresent(String expectedPageTitle, Account user) {
        Logger.info("Verify if page " + expectedPageTitle + " presents");
        MailboxBasePage mailbox = new MailboxBasePage();
        MailLoginErrorPage mailLoginError = new MailLoginErrorPage();
        boolean result = false;
        if (expectedPageTitle.equals("Inbox")) {
            result = mailbox.isInboxPagePresent(user);
        }
        if (expectedPageTitle.equals("YandexPassport")) {
            result = mailLoginError.isYandexPassportPagePresent();
        }
        return result;
    }

}
