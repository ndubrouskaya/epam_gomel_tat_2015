package com.epam.gomel.tat.reporting;

/**
 * Created by Natashka on 11.04.2015.
 */
public class Logger {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public synchronized static void fatal(String s) {
        log.fatal(s);
    }

    public synchronized static void error(String s) {
        log.error(s);
    }

    public synchronized static void warn(String s) {
        log.warn(s);
    }

    public synchronized static void info(String s) {
        log.info(s);
    }

    public synchronized static void debug(String s) {
        log.debug(s);
    }

    public synchronized static void trace(String s) {
        log.trace(s);
    }

    public synchronized static void fatal(String s, Throwable t) {
        log.fatal(s, t);
    }

    public synchronized static void error(String s, Throwable t) {
        log.error(s, t);
    }

    public synchronized static void warn(String s, Throwable t) {
        log.warn(s, t);
    }

    public synchronized static void info(String s, Throwable t) {
        log.info(s, t);
    }

    public synchronized static void debug(String s, Throwable t) {
        log.debug(s, t);
    }

    public synchronized static void trace(String s, Throwable t) {
        log.trace(s, t);
    }
}
