package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.exceptions.CommonRuntimeException;
import com.epam.gomel.tat.pages.LetterContentPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;

/**
 * Created by Natashka on 12.04.2015.
 */
public class MailService {

    public void sendMail(Letter letter) {
        Logger.info("Send mail to " + letter.getReceiver() + " with subject " + letter.getSubject());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxListPage().openComposeMail().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent());
        if (!mailbox.isMessageMailSentPresent()) {
            throw new CommonRuntimeException("No message that mail was success sent");
        }
    }

    public void isLetterInSent(Letter letter) {
        Logger.info("Check if sent mail with subject " + letter.getSubject() + " present in sent list");
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterContent = mailbox.openSentListPage().openLetter(letter.getSubject());
        String receiver = letterContent.getLetterReceiver();
        String subject = letterContent.getLetterSubject();
        String content = letterContent.getLetterContent();
        StringBuilder errorBuilder = new StringBuilder();
        if (receiver == null || !receiver.equals(letter.getReceiver())) {
            errorBuilder.append(subject + " - is incorrect! Receiver of the sent mail: '" + letter.getReceiver() + "'");
        }
        if (subject == null || !subject.equals(letter.getSubject())) {
            errorBuilder.append(subject + " - is incorrect! Subject of the sent mail: '" + letter.getSubject() + "'");
        }
        if (content == null || !content.equals(letter.getContent())) {
            errorBuilder.append("Content of sent mail is incorrect!");
        }
        if (!errorBuilder.toString().equals("")) {
            Logger.error(errorBuilder.toString());
            throw new CommonRuntimeException(errorBuilder.toString());
        }
    }
}
