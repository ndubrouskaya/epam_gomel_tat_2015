package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 13.04.2015.
 */
public class ComposeMailPage {
    public static final By TO_INPUT_FIELD_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_FIELD_LOCATOR = By.name("subj");
    public static final By MAIL_TEXTAREA_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        Browser.get().type(TO_INPUT_FIELD_LOCATOR, mailTo);
        Browser.get().type(SUBJECT_INPUT_FIELD_LOCATOR, mailSubject);
        Browser.get().type(MAIL_TEXTAREA_LOCATOR, mailContent);
        Browser.get().click(SEND_MAIL_BUTTON_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new MailboxBasePage();
    }

}
