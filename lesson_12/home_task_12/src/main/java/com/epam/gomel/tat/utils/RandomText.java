package com.epam.gomel.tat.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Natashka on 13.04.2015.
 */
public class RandomText {

    public static String randomStringLong() {
        return RandomStringUtils.randomAlphabetic(100);
    }

    public static String randomStringShort() {
        return RandomStringUtils.randomAlphabetic(5);
    }
}
