package com.epam.gomel.tat.steps;

import com.epam.gomel.tat.ui.Browser;
import org.jbehave.core.annotations.AfterScenario;

/**
 * Created by Natashka on 14.04.2015.
 */
public class AfterScenarioSteps {
    @AfterScenario
    public void afterScenario() {
        Browser.kill();
    }
}
