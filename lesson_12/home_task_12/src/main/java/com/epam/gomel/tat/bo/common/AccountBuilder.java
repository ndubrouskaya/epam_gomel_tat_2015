package com.epam.gomel.tat.bo.common;

/**
 * Created by Natashka on 11.04.2015.
 */
public class AccountBuilder {

    public static Account createUser() {
        return new Account();
    }

    public static Builder build(Account user) {
        return new Builder(user);
    }

    public static class Builder {

        Account accountToBuild;

        public Builder(Account user) {
            accountToBuild = user;
        }

        public Builder withLogin(String login) {
            accountToBuild.setLogin(login);
            return this;
        }

        public Builder withPassword(String password) {
            accountToBuild.setPassword(password);
            return this;
        }
    }
}
