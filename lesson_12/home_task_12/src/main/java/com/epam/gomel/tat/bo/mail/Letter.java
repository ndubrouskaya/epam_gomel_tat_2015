package com.epam.gomel.tat.bo.mail;

/**
 * Created by Natashka on 13.04.2015.
 */
public class Letter {
    private String receiver;
    private String subject;
    private String content;

    public Letter(String receiver, String subject, String content) {
        this.setReceiver(receiver);
        this.setSubject(subject);
        this.setContent(content);
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
