package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 11.04.2015.
 */
public class MailboxBasePage {

    public static final By INBOX_LINK_LOCATOR = By.xpath("//div[@class='block-left-box']//a[@href='#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By MESSAGE_MAIL_SENT_LOCATOR = By.xpath("//div[@class='b-done-title'][contains(text(),'Письмо успешно отправлено.')]");
    public static final String INBOX_BLOCK_MESSAGES_LOCATOR = ("//span[contains(text(),'%s')]//ancestor::div[@class='block-messages']");

    public MailInboxListPage openInboxListPage() {
        Browser.get().click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSentListPage openSentListPage() {
        Browser.get().click(SENT_LINK_LOCATOR);
        return new MailSentListPage();
    }

    public boolean isInboxPagePresent(Account user) {
        return Browser.get().isPresent(By.xpath(String.format(INBOX_BLOCK_MESSAGES_LOCATOR, user.getLogin())));
    }

    public boolean isMessageMailSentPresent() {
        return Browser.get().isPresent(MESSAGE_MAIL_SENT_LOCATOR);
    }
}
