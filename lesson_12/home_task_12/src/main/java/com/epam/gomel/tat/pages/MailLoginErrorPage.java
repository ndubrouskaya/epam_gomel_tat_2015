package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 12.04.2015.
 */
public class MailLoginErrorPage {

    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='error-msg'][contains(text(),'Неправильная пара логин-пароль!')]");
    public static final By LOGIN_BLOCK_CAPTCHA_LOCATOR = By.xpath("//div[@class='domik']");

    public boolean isYandexPassportPagePresent() {
        return (Browser.get().isPresent(ERROR_MESSAGE_LOCATOR) || Browser.get().isPresent(LOGIN_BLOCK_CAPTCHA_LOCATOR));
    }

}
