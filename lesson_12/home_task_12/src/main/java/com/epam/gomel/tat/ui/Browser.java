package com.epam.gomel.tat.ui;

import com.epam.gomel.tat.reporting.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Natashka on 11.04.2015.
 */
public class Browser {
    public static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt, image/jpeg";
    public static final int WAIT_ELEMENT_TIMEOUT = 20;
    public static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int AJAX_TIMEOUT = 20;
    private static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    public static final String DOWNLOAD_DIR = FileUtils.getTempDirectoryPath();
    private WebDriver driver;

    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    private static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public static void kill() {
        Logger.info("Close browser");
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instance = null;
            }
        }
    }

    public void takeScreenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
            FileUtils.copyFile(screenshot, copy);
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public void open(String url) {
        Logger.info("Open URL: " + url);
        driver.get(url);
        takeScreenshot();
    }

    public void click(By locator) {
        Logger.info("Click " + locator);
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        element.click();
        takeScreenshot();
    }

    public void type(By locator, String text) {
        Logger.info("Type " + text + " into " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public void submit(By locator) {
        Logger.info("Submit " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        element.submit();
    }

    public String getText(By locator) {
        Logger.info("GetText from " + locator.toString());
        waitForPresent(locator);
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        takeScreenshot();
        return element.getText();
    }

    public boolean isPresent(By locator) {
        Logger.info("Checking if element " + locator.toString() + " present");
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present " + locator.toString());
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForAjaxProcessed() {
        Logger.info("Wait for Ajax reguests");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

}
