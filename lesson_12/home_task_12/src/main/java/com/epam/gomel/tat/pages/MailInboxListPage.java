package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 13.04.2015.
 */
public class MailInboxListPage {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    public ComposeMailPage openComposeMail() {
        Browser.get().click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }
}
