package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 13.04.2015.
 */
public class MailSentListPage {

    public static final String SENT_SUBJECT_LOCATOR_PATTERN = "//label[text()='Отправленные']//ancestor::div//span[@class='b-messages__subject'][text()='%s']";

    public LetterContentPage openLetter(String text) {
        Browser.get().click(By.xpath(String.format(SENT_SUBJECT_LOCATOR_PATTERN, text)));
        return new LetterContentPage();
    }
}
