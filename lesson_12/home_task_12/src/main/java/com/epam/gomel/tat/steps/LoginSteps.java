package com.epam.gomel.tat.steps;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.LoginService;
import com.epam.gomel.tat.ui.Browser;
import junit.framework.Assert;
import org.apache.http.util.Asserts;
import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;
import org.junit.AfterClass;

/**
 * Created by Natashka on 11.04.2015.
 */
public class LoginSteps extends Steps {

    private LoginService loginService = new LoginService();
    private Account user;

    @Given("User")
    public void userIsMailBoxOwner() {
        user = AccountBuilder.createUser();
    }

    @Given("User has correct login $login and correct password $password")
    @Alias("User has correct login $login and incorrect password $password")
    public void userHasLoginAndPassword(String login, String password) {
        AccountBuilder.build(user).withLogin(login).withPassword(password);
    }

    @When("User logs to his mailbox")
    public void userLogsToHisMailbox() {
        loginService.loginToMailbox(user);
    }

    @Then("Page with $pageTitle mail opens")
    @Alias("Page $pageTitle opens")
    public void isPagePresent(String expectedPageTitle) {
        Assert.assertTrue("Incorrect page!", loginService.isExpectedPagePresent(expectedPageTitle, user));
    }


}
