package com.epam.gomel.tat.steps;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginService;
import com.epam.gomel.tat.service.MailService;
import com.epam.gomel.tat.ui.Browser;
import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;

/**
 * Created by Natashka on 13.04.2015.
 */
public class SendMailSteps extends Steps {

    private LoginService loginService = new LoginService();
    private Account user;
    private Letter letter;
    MailService mailService = new MailService();

    @Given("User logs to his mailbox with login $login and password $password")
    public void userLogsToHisMailbox(String login, String password) {
        user = AccountBuilder.createUser();
        AccountBuilder.build(user).withLogin(login).withPassword(password);
        loginService.loginToMailbox(user);
    }

    @Given("User has letter to send")
    public void userHasLetterToSend() {
        letter = LetterBuilder.getLetter();
    }

    @When("User send letter")
    public void userSendLetter() {
        mailService.sendMail(letter);
    }

    @Then("Sent letter is in the sent list")
    public void isLetterInTheSentList() {
        mailService.isLetterInSent(letter);
    }

}
