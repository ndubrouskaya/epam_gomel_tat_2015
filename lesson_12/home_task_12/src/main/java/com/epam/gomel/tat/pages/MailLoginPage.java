package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Natashka on 11.04.2015.
 */
public class MailLoginPage {
    public static final String BASE_URL = "http://www.mail.yandex.ru";

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//span[contains(@class, 'auth-form')]/button");

    public MailLoginPage open() {
        Browser.get().open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        Browser.get().type(LOGIN_INPUT_LOCATOR, login);
        Browser.get().type(PASSWORD_INPUT_LOCATOR, password);
        Browser.get().submit(ENTER_BUTTON_LOCATOR);
        return new MailboxBasePage();

    }
}

