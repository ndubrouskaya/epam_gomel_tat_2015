package com.epam.gomel.tat.bo.mail;

import com.epam.gomel.tat.utils.RandomText;

/**
 * Created by Natashka on 13.04.2015.
 */
public class LetterBuilder {

    public static String receiver = "natashka.test@yandex.ru";

    public static Letter getLetter() {
        Letter letter = new Letter(receiver, RandomText.randomStringShort(), RandomText.randomStringLong());
        return letter;
    }
}
