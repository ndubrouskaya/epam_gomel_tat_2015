package com.epam.gomel.tat.exceptions;

/**
 * Created by Natashka on 13.04.2015.
 */
public class CommonRuntimeException extends RuntimeException {

    public CommonRuntimeException(String message) {
        super(message);
    }

}
