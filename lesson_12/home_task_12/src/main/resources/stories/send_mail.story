Feature: Send mail
Meta:
@send

As a Mailbox User
I want to send letter
So that I can see it in sent list

Scenario: Send mail
Given User logs to his mailbox with login natashka.test and password ntcnjdsq
And User has letter to send
When User send letter
Then Sent letter is in the sent list