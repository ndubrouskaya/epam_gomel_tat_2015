    [info] Playing test case belavia
    [info] Executing: |open | /home/ | |
    [info] Executing: |select | id=langs_uni | label=Русский |
    [info] Executing: |clickAndWait | css=input[type="submit"] | |
    [info] Executing: |sendKeys | id=origin | Минск |
    [info] Executing: |click | css=#ui-active-menuitem > strong | |
    [info] Executing: |sendKeys | id=destination | Прага |
    [info] Executing: |click | css=#ui-active-menuitem > strong | |
    [info] Executing: |click | id=departureCalendar | |
    [info] Executing: |click | link=29 | |
    [info] Executing: |click | id=returnCalendar | |
    [info] Executing: |click | link=31 | |
    [info] Executing: |click | css=img.ar-top | |
    [info] Executing: |clickAndWait | css=div.btn-wdt > input[type="submit"] | |
    [info] Executing: |storeText | css=span.today | date |
    [info] Executing: |storeText | css=div#outbound>div.t-body>div.t-row:nth-child(1)>div.flight-avail>div.departure>strong | time_departure1 |
    [info] Executing: |storeText | css=div#outbound>div.t-body>div.t-row:nth-child(1)>div.flight-avail>div.arrival>strong | time_arrival1 |
    [info] Executing: |storeText | css=div.er.fare > label | economy_restrict1 |
    [info] Executing: |storeText | css=div.sf.fare > label | economy_semi-flex1 |
    [info] Executing: |storeText | css=div.ef.fare > label | economy_flex1 |
    [info] Executing: |storeText | css=div.bc.fare > label | business1 |
    [info] Executing: |echo | ${date} | |
    [info] echo: воскресенье 29 марта
    [info] Executing: |echo | ${time_departure1} | |
    [info] echo: 12:20
    [info] Executing: |echo | ${time_arrival1} | |
    [info] echo: 13:05
    [info] Executing: |echo | ${economy_restrict1} | |
    [info] echo: 1 473 300 BYR
    [info] Executing: |echo | ${economy_semi-flex1} | |
    [info] echo: ${economy_semi-flex1}
    [info] Executing: |echo | ${economy_flex1} | |
    [info] echo: 4 501 750 BYR
    [info] Executing: |echo | ${business1} | |
    [info] echo: 4 984 675 BYR
    [info] Test case passed 